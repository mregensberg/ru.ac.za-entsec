# Enterprise Security
### *DAY 1 MORNING PART 1*

Risk Identification - articulating the risks. Understand the business first.
Consider and read the ISO 27001 Management System Directives

Process Presentation
    * Determine Risks & Articulate
    * Determine Controls & Articulate
    * Etc..

Start with a 90 day plan (or 100 day plan, to keep it trump-ish). Objectives:

    * _Business Discovery_ - focus on articulating the value chain.
        * *Value Chain* (what is the value in the value chain - information in the case of a consultancy or data business)
        * *Customer View* - what are the security touchpoints for a customer
        * An example would be ensuring customers cant see each others data (this would feed into the risk model). Focus on awareness programs for ethics, integrity etc.
        * *Org Structure* (map to the value chain) > Business Context Document
        * *System Information* (map to the value chain) > Business Context Document
        * Value chain example in a bank e.g. -> Sign up customer, Take Money, Re-lend the money etc. How do you prevent the rabbit hole? You only need to know enough to move on.
        * CIA (confidentiality/integrity/availability) -> consider, but not necessarily document
        
    * _High level risk assessment_ (given what I know)
        * What is risk - uncertainty introduced into achieving a given objective (ISO 31000 standards). Threat and probability. Look at the intel threat library (Johann will email spreadsheet version of this)
        * Threat Management
        [file:316B22CF-DCF1-4966-B3B3-E07D3596A579-273-0000D4BFC0ADA01F/Threat Agent Library_07-2202w.pdf]
        [file:144CAF75-8D97-47DB-B4E9-B0F424A64039-273-0000D5060D654E11/wp_IT_Security_RiskAssessment.pdf]
        * Security Capacity (controls, frameworks, what currently exists - look at security at a component level. Endpoints, awareness training etc.)
        * Penetration Test - motivates implementation of controls
        * Outcome is a health check / capability maturity model
    * _Deeper Dive to assess the detailed risk_ and compensating control
    * Detailed Risk Assessment can be (and should be) phased, and viewed in the context of a threat model (ref. intel threat model. attack trees. killchains - [Academic: Attack Trees - Schneier on Security](https://www.schneier.com/academic/archives/1999/12/attack_trees.html) ). What vulnerabilities need to exist to make this possible.
    * To get hold of the standard controls, e.g. 270002. Validate your thinking against the list.
    * *_Overall Outcome: Determining the ISMS Scope_*


### *DAY 1 MORNING PART 2*

Risk assessment should also be operationalized (from a 27000 perspective, it’s positioned as a planning tool) in order to keep them updated.

From the threat model - Vulnerabilities & Impact (likelihood)?



